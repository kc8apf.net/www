---
title: Where do I get advice on Yocto?
date: 2016-12-27T23:25-08:00
categories:
  - Dear Brother Yocto
---
Over the past three years, I've worked on a variety of projects that use embedded Linux distros built using Yocto Project. While I quickly realized the versatility of Yocto during the first project, learning the ins and outs of recipes, distros, and machines became a regular frustration that followed through each project. Not only is Yocto huge, the design patterns and tools change almost daily.

<!-- more -->

[Yocto Project Reference Manual] and [BitBake User Manual] contain a wealth of vital information about the core concepts and well-established tools but traditional documentation only gets you so far. As my understanding of Yocto grew, mostly by reading vast amounts of code while debugging problems, I became the local go-to expert in my teams. Instead of hacking away at the next task, I was answering questions whose answers required understanding how and why things were designed the way they were, information only available in the code itself. Yet there was nowhere to persist these conversations.

That's how I came to start this series I'm calling "Dear Brother Yocto." Originally I just wanted a place to share tips and tricks as I learn them. By the time I got the site up and running, I had seen enough requests for advice on the Yocto mailing list that having a write-in advice column style just made sense. Some posts will be from readers, some will be from my own questions. Regardless, I'll dig into the code, find a solution, and explain what the heck is going on.

If you have a question about Yocto, please share it with me via Twitter ([@kc8apf](https://twitter.com/kc8apf)) or email (kc8apf at kc8apf dot net).

[Yocto Project Reference Manual]: http://www.yoctoproject.org/docs/2.2/ref-manual/ref-manual.html
[BitBake User Manual]: http://www.yoctoproject.org/docs/2.2/bitbake-user-manual/bitbake-user-manual.html