---
title: On wiring diagrams
date: 2017-07-16T14:31-07:00
updated: 2017-07-17T10:37-07:00
categories:
  - Car Hacking
---
After I finished rewiring the Cobra drag car's trunk, I found myself needing to pull a fuse to do a test and not remembering which fuse was for what. Having all the fuses and relays in one panel was a huge improvement over the original wiring but clearly it was time for labels and a wiring diagram.

<!-- more -->

# Shopping for one-off labels
These labels are going to be affixed near the fuel cell and will likely see some abuse. I want a label that has a strong adhesive, is resistant to scratching, is UV resistant, and tolerates contact with fuel. This is no ordinary sticker. This is a high quality label.

I know [Sticker Mule](https://www.stickermule.com/) makes custom stickers. Maybe they have something that would work. Bad sign #1: [minimum order quantity](https://www.stickermule.com/products/rectangle-stickers). I will need precisely one of each label. Having 49 extras is a bit much. A few quick searches show any custom label order is going to have a minimum order. It makes sense: setup takes time. Time to rethink my approach.

From somewhere in the back of my mind, the name [Avery](http://www.avery.com/) jumped forth. Avery makes printable labels in all sorts of shapes and sizes formatted on standard paper sizes so any inkjet or laser printer can be used. I fondly remember churning out hundreds of their mailing labels in the 90's by running a Word mail merge and using Avery's provided templates. Maybe I can put a coating over their labels.

<figure>
<img src="{% asset_path Avery-UltraDuty-GHS-Labels.jpeg %}" alt="Avery UltraDuty GHS Chemical Labels box">
</figure>

No need! Avery makes [UltraDuty GHS Chemical Labels](http://www.avery.com/avery/en_us/Products/Industrial-Solutions/UltraDuty-GHS-Chemical-Labels/GHS-Overview.htm) designed for labeling chemical containers. They are promoted as great for custom [NFPA](http://www.nfpa.org/704) and [HMIS](https://en.wikipedia.org/wiki/Hazardous_Materials_Identification_System) labels. That should work perfectly in a trunk. Oh, look! They even have a [template](http://www.avery.com/avery/en_us/Search/?dimsearch=false&amp;N=0&amp;Ntk=SoftwareAndTemplate&amp;Ntx=mode+matchall&amp;Nty=0&amp;Nr=SITESCHANNELS:Avery.com:Templates+%26+Software:Templates&amp;Ns=Rank%7C0&amp;Ntt=60502&amp;int_id=averyproduct-viewtemplates-60502)! Wait... it's in Word? I don't even own a copy of Word anymore. Hmm, what's this? They have an [online tool](http://www.avery.com/avery/en_us/Templates-%26-Software/Software/Avery-Design--Print-Online.htm?int_id=topnav-templates-DPOstartpage) instead? Ok. I'm generally against these kinds of tools but I'm getting desperate.  Five minutes later, I close the tab in frustration. Well, I have a label but I'm on my own to layout my designs.

# Troublesome Tools
I am a programmer, not an artist.  I know how to draft with pencil, paper, and ruler but I much rather write code.  [SVG](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics) seemed like a natural fit for the lineart and text I planned to use on these labels.  Not having used SVG before, I read a few [how-to](https://www.w3schools.com/graphics/svg_intro.asp) articles before diving into the [specification](https://www.w3.org/TR/SVG11/).  Laying out the basic shape of the label was pretty easy but filling it with content became baffling. There were so many options of how to proceed with different ramifications.  Two evenings of trying to figure it out was all I could muster.  I deleted my work in progress and took a day off.

As I took some time working on other projects, I realized that wiring diagrams are a form of undirected graph rendered orthographically. [GraphViz](http://www.graphviz.org/) is the common tool used by programmers far and wide for this.  I quickly wrote up a few lines of [DOT](http://www.graphviz.org/content/dot-language) that represented some of the major components in my diagram (battery, emergency disconnect, fuse panel) and the +12V and ground wiring between them.  GraphViz, specifically neato, generated a diagram that was technically correct but looked horrible.  Even with ortho layout selected, lines were going underneath boxes, boxes were laid out nonsensically.  As I researched how to pin boxes to specific locations in the diagram and use shapes other than boxes, I discovered these were mostly kludges.  Yet another tool was requiring huge amounts of fiddling to get anything close to reasonable output.  Ugh!

# Fine.  I'll draw it if I have to.
I'm a Mac user.  Part of being a Mac user is discovering [The Omni Group](https://www.omnigroup.com/)'s fabulous collection of tools.  If I'm going to draw a wiring diagram, I'm going to do with [OmniGraffle](https://www.omnigroup.com/omnigraffle), a tool similar in concept to [Visio](https://products.office.com/en-us/Visio/?tab=tabs-1).  In an evening, I drew both a label for the fuse panel and a wiring diagram for the trunk.  By grouping primitive shapes, I could create complex shapes that help with identifying components.  Adding magnet points to those shapes anchors the ends of lines (wires) that track as I moved the shapes and line path around.  For all that I wanted to avoid drawing, the process ended up being straightforward and effective.

<figure>
<img src="{% asset_path Trunk-Labels.png %}">
<figcaption>Cobra Commander trunk labels made with OmniGraffle</figcaption>
</figure>

I'll post pictures of the installed labels when I get a chance.