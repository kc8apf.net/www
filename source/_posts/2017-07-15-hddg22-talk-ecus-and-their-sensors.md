---
title: "HDDG22: Talk ECUs and their sensors"
date: 2017-07-15T20:51-07:00
updated: 2017-07-17T10:40-07:00
categories:
  - Car Hacking
---
[Chris Gammell](https://twitter.com/Chris_Gammell) asked me to give a presentation at his [Hardware Developers Didactic Galactic](https://www.meetup.com/Hardware-Developers-Didactic-Galactic/) meetup in San Francisco.  I enjoy talking about things I work on so I didn't hesitate to say yes.  I'm pretty sure he was expecting me to talk about Google's [Zaius](https://github.com/opencomputeproject/zaius-barreleye-g2) server, an open-hardware POWER9 server design,  that I brought to a previous meetup.  Giving presentations about my day job takes some review and approvals, even for open designs.   Instead, I offered to talk about engine control, a subject I'm spending many nights on recently.

<!-- more -->

I've had to learn about engine tuning and EFI systems in particular to get Cobra Commander back on the track.  Since that knowledge seems to be spread in bits and pieces, I put together what I've learned into a presentation focusing on the sensors used and how they fit into the engine control systems.

@[youtube](qV3KnpuRdQk)

[Slides](https://docs.google.com/presentation/d/1w0iI1Rw7jM9PNp9Ajuhy02fsTxLJFGxPn6J-cyGTxWE/edit#slide=id.p) and a [writeup](https://medium.com/supplyframe-hardware/understanding-how-cars-work-by-looking-at-the-internal-engine-sensors-4ba6daa1fe6a) for SupplyFrame's blog are also available.