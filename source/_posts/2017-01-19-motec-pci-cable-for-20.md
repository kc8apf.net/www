---
title: "MoTeC PCI Cable for $20"
date: 2017-01-19T01:01-08:00
updated: 2018-07-13T09:23-08:00
categories:
  - Car Hacking
---
::: update
Preassembled cables are available at [Tindie](https://www.tindie.com/products/kc8apf/usb-m8/) as of 2018-07-13.
:::

<!-- block -->
After getting the MoTeC M48 EMP software installed in Dosbox as described [previously](drag-race-cobra-and-motec-m48-ecu), I naively assumed the M48 just connected with normal RS232 serial.  I mean, there's a DB9 hanging under the dash and the EMP software is looking for a serial port.  Of course it couldn't be that simple.

<!-- block -->

<figure>
<img src="{% asset_path motec-pci-cable.jpg %}" alt="Grey cable with DE-9 connectors on both ends. One end has a black connector housing. The other has a chrome housing.">
<figcaption>Motec PCI Cable: Pure Unobtainium</figcaption>
</figure>

It seems that MoTeC decided to save a dollar in the M48 by not including RS232 level shifters.  Instead, it has 5V TTL serial on non-standard pins.  MoTeC helpfully offers [cables](http://www.motec.com/ac-cc-seriallogiclevel/cc-seriallogic-pc/), called PCI and CIM, to bridge the gap from an RS232 serial port to the ECU.  The downside? These cables are practically unobtainium (or at least cost [$220 for one](http://johnreedracing.com/communications/motec-m48/m8-communication-cable-6ft/)).  Knowing that it's just TTL serial, why is this cable so expensive?  Can't I just make one?

<figure>
<img src="{% asset_path pc-connector-pinout.png %}" alt="Schematic showing a male DE-9 connector's pins and their connections to a MoTeC M48 ECU connector">
<figcaption>Official wiring diagram for M48 serial port</figcaption>
</figure>

MoTeC offers a [wiring diagram](http://www.motec.com/filedownload.php/ecu-m48.pdf?docid=1210) for the M48 but it doesn't tell you what signals are on the DB9 pins.  At least they were clear that pin 6 is ground.  After some time with a voltmeter and oscilloscope, I figured out the following pinout:

|DB9|Signal|ECU|
|---|------|---|
| 1 | +5V  | 24|
| 2 |  N/C |
| 3 |  N/C |
| 4 |  N/C |
| 5 |  TXD | 11|
| 6 |  GND | 27|
| 7 |  ??? | 12|
| 8 |  N/C |
| 9 |  RXD |  9|

<figure>
<img src="{% asset_path ftdi-cable.jpg %}" alt="Black cable with a USB Type A connector on one end and a 0.1 inch 1 by 6 connector on the other end">
<figcaption>Sparkfun FTDI Cable 5V: Functional equivalent to MoTeC PCI cable</figcaption>
</figure>

So, what's inside that $220 cable?  Probably a [MAX232](http://www.ti.com/lit/ds/symlink/max232.pdf).  A simpler, modern alternative is a [FTDI 5V cable](https://www.sparkfun.com/products/9718) from Sparkfun for $20.  Either make a connector to adapt to DB9 or wire directly into the ECU connector pins.

Now the M48 EMP software running in Dosbox is talking to the ECU over the FTDI cable.  I've successfully downloaded the current tune and some log data.  I even discovered that the air temperature sensor was showing a fault and was able to clear it by reseating the connector on the sensor.

Next up is inventorying the sensors and actuators installed and finding their datasheets.  The goal is to get a good understanding of the tune that is currently loaded and get ready for logging data during dyno runs.  That probably means building a board that can capture the telemetry the ECU spews out the serial port during normal operation.  Then I'll start digging into the protocol used by EMP to configure the ECU.