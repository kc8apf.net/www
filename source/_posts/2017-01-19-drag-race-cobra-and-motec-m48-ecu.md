---
title: Drag Race Cobra and MoTeC M48 ECU
date: 2017-01-19T01:00-08:00
updated: 2017-07-17T10:49-07:00
categories:
  - Car Hacking
---
A few month ago, a coworker decided to buy a built drag racing car and was looking for someone to help crew.  I emphatically offered my services and we started working out what exactly he had bought.

<!-- more -->

The car started as a stock 1996 Ford Mustang Cobra with a 4.6L V8 engine (in Mystic, no less).  The engine has been rebuilt with a supercharger and nitrous.  The drivetrain has been completely replaced.  Yet, it still has power windows, doors, and steering.  The story from the seller was that the original owner had some outrageous tuning that needed the nitrous for cooling, not power.  The seller, the 2nd owner, had had a professional tuner redo the tuning from scratch to get something that would work as a daily driver, an 800HP daily driver.  It's an interesting beast to say the least.

<figure>
<img src="{% asset_path Motec-M48.jpg %}" alt="Gold-colored metal box labeled MoTeC M48">
<figcaption>MoTeC M48</figcaption>
</figure>

I started looking into the ECU to figure out what we had to work with.  It's a [MoTeC M48](http://www.motec.com/filedownload.php/M4%20M48%20Brochure.pdf?docid=2389) which is a well-respected ECU from the 90s.  MoTeC still has the manual and software available for download but that's where this story starts to get annoying.  The M48 software is a DOS-only application that talks to the ECU over serial.  Most of the applications will work under Windows 7 but not all.  I couldn't get it to work with Windows 10 at all.  After a bit of research, others had had luck with [Dosbox](http://www.dosbox.com/).

<figure>
<img src="{% asset_path Screen-Shot-2017-01-19-at-12.20.30-AM.png %}">
<figcaption>Screenshot of MoTeC M48 EMP software</figcaption>
</figure>

I use a MacBook Air so I tried [Boxer](http://boxerapp.com/) which is just a nice UI over Dosbox.  Installing the MoTeC software is...odd.  The [download](http://www.motec.com/filedownload.php/m48_620d.exe?docid=2404) from MoTeC is a Windows installer that dumps a DOS-based installer into C:\Motec.  The DOS-based installer is built around floppy images and is generally quirky.  I was able to successfully get everything installed in a Windows Vista VM I had handy and then copy C:\Motec into a Dosbox disk image.  If you are looking to use MoTeC M48 EMP on a Mac and want to save a bunch of time, email me.

One last note about using Boxer: the Boxer UI doesn't expose any settings for serial ports.  Do not despair; Boxer's Dosbox core has all the serial port support included.  You just need to add the following to the DOSBox Preference.conf stored inside the Boxer disk image bundle (right-click on the disk image in Finder and click Show Package Contents):
```
[serial]
serial1=directserial realport:cu.usbserial-A100P1XB
```
Change _cu.usbserial-A100P1XB_ to the serial port of your choice (one of /dev/cu.*) and you'll be good to go.