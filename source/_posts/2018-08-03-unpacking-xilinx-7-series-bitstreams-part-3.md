---
title: "Unpacking Xilinx 7-Series Bitstreams: Part 3"
date: 2018-08-03T10:19-07:00
categories:
  - FPGA Hacking
---
In [Part 2](unpacking-xilinx-7-series-bitstreams-part-2), I detailed the configuration packet format and how the programming operation is conveyed as a sequence of register writes. As we move up to the configuration memory frame layer (see layer diagram in [Part 1](unpacking-xilinx-7-series-bitstreams-part-1)), the  construction of a Xilinx 7-series device becomes important. A major clue about this relationship comes from the Frame Address Register, a key register in any programming operation.

<!-- more -->

# Frame Address Register (FAR)
Just before the first write of a configuration frame to the Frame Data Register, Input Register (FDRI), a 32-bit value is written to the Frame Address Register (FAR) to indicate where the new frame data should be placed in the configuration memory. What do these addresses tell us about the device construction?  [UG470](https://www.xilinx.com/support/documentation/user_guides/ug470_7Series_Config.pdf) tells us these 32-bit addresses are comprised of the following fields:

<figure>
<img src="{% asset_path frame_address_register.svg %}">
<figcaption>Xilinx 7-Series Frame Address Register</figcaption>
</figure>

Blocks are further described as only using a few specific values:

| Encoding | Block Type |
|----------|------------|
| 000 | CLB, I/O, CLK |
| 001 | Block RAM content |
| 010 | CFG_CLB |

This looks very much like a geographical addressing scheme similar to the Bus/Device/Function scheme used for [PCI Configuration Space](https://en.wikipedia.org/wiki/PCI_configuration_space). That is, the device is constructed of a hierarchy of component groupings. In the case of PCI, a system may contain 256 buses, each of which may contain up to 32 devices.  Further each device may contain up to 8 functions. Identifying a specific function requires identifying the bus and device that contain it as well. Thus, function addresses in PCI are a tuple of (bus, device, function). Balancing complexity of address decoding logic with address compactness leads to representing each component of the tuple as a binary number with the minimum number of bits needed to represent the maximum allowed value and then concatenating those numbers into a single binary number padded to a common alignment size (8, 16, 32, or 64 bits).

# Inferring Device Architecture
What does this tell us about 7-series devices then? A device is constructed of some hierarchy of block types, device halves, rows, columns, and minor frames. The FAR field descriptions of [UG470](https://www.xilinx.com/support/documentation/user_guides/ug470_7Series_Config.pdf) gives us a few more details:

* Rows are numbered outward from the device center-line in the direction specified by the top/bottom bit
* Columns are numbered with zero on the left and increasing to the right
* Minor frames are contained within a column

Looking back at the FAR description, it seems that the fields are ordered such that each component contains all the components to the right of its field in FAR. That matches with traditional meanings of the terms used _except_ the relationship between block types and halves. If rows are numbered growing outward from the center-line of the device, that implies there are only two halves in a device, not two per block type. Recall that only three block type values are used. What if instead of being part of the hierarchy, the block type selects one of multiple data buses going into the hierarchy? That would match the terms used better.

Combining those conclusions with the field bit-widths in FAR, we end up with the following addressing limits:

|||
|-|-|
| **Block Types** | 3 |
| **Halves** | 2 |
| **Rows** | up to 32 per half |
| **Columns** | up to 1024 per row |
| **Minor frames** | up to 128 per column |

Putting all of that together, the device looks something like this:

<figure>
<img src="{% asset_path config_mem_layout_overview_initial-1.svg %}">
</figure>

# Next: Verifying Against a Bitstream
In Part 4, I'll look at the FAR and FDRI writes done by a Vivado-generated bitstream and see how well it matches my inferred device architecture. I'll use a bitstream debugging feature to figure out the valid frame addresses in a device which results in a few surprises.